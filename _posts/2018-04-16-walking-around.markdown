---
layout: post
title:  "Walking Around"
date:   2018-04-16 22:22:00 +0200
categories: blog farm outdoors
author: jonathan
---
I took a walk the other day around the farm.

Here are some photos from my walk.

![Panorama]({{ "/assets/blog_images/walking_around_compressed/view.jpg" | absolute_url }})

A pretty panorama :).

![The Gate]({{ "/assets/blog_images/walking_around_compressed/tree.jpg" | absolute_url }})

Tree!

![Barbed Wire]({{ "/assets/blog_images/walking_around_compressed/fence.jpg" | absolute_url }})

Some barbed wire fence.

![A Gate]({{ "/assets/blog_images/walking_around_compressed/gate.jpg" | absolute_url }})

A gate.

![The Gate]({{ "/assets/blog_images/walking_around_compressed/road.jpg" | absolute_url }})

Looking up the road.

![The Gate]({{ "/assets/blog_images/walking_around_compressed/sign.jpg" | absolute_url }})

The lapa.

![The Gate]({{ "/assets/blog_images/walking_around_compressed/trees.jpg" | absolute_url }})

Some more trees looking over the mostly dried up lake. We definitely need some rain soon.

![The Gate]({{ "/assets/blog_images/walking_around_compressed/shadow.jpg" | absolute_url }})

Me!
