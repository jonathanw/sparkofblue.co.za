---
layout: post
title:  "Shelves and Things"
date:   2018-04-22 08:33:00 +0200
categories: blog home inside
author: jonathan
---
Our house has very little storage space, so a little while ago I bought 
a drill and some tools and started putting shelves on the wall. 

These are some of them.

![Little Shelf]({{ "/assets/blog_images/shelves/thefirstone.jpg" | absolute_url }})

This is just a little shelf in the lounge.

![Starting]({{ "/assets/blog_images/shelves/start.jpg" | absolute_url }})

Just starting out. This little area of wasted space makes a very nice area 
to put up a few shelves.

![Almost done]({{ "/assets/blog_images/shelves/almost.jpg" | absolute_url }})

Getting there, almost done now.

![Done]({{ "/assets/blog_images/shelves/done.jpg" | absolute_url }})

And, done! Even put some things on them.

And then for something different I got a nice spice rack for the wife. It is 
made out of an old palette.

![Spicey]({{ "/assets/blog_images/shelves/spice.jpg" | absolute_url }})
