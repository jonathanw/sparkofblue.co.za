---
layout: post
title:  "Paternoster"
date:   2018-04-04 16:42:00 +0200
categories: blog travel roadtrip drive
author: jonathan
---
Good Friday seemed like a wonderful day to take a road trip out to the West Coast.

![The view]({{ "/assets/blog_images/29404377_548076015573651_4343247710569103360_n.jpg" | absolute_url }})

This was the view that greeted us.

We ate lunch at the Seekombuis, which is just inside the Cape Columbine Nature Reserve. If you tell the guards at the gate you're going to the restaurant you don't get charged an entrance fee.

![Titiesbaai]({{ "/assets/blog_images/29403747_2043620105879776_6915746221076250624_n.jpg" | absolute_url }})

Of course I had to get one of these hats.
