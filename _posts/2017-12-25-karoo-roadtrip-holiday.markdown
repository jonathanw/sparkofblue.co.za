---
layout: post
title:  "Karoo Roadtrip Holiday"
date:   2017-12-25 09:03:00 +0200
categories: blog travel roadtrip drive december holiday leave family
author: jonathan
---
[![Map]({{ "/assets/blog_images/roadtrip_december_2017/map.png" | absolute_url }})](https://drive.google.com/open?id=1ACbkjy4vVaBSG6atScJaJTf1MxD71P6U&usp=sharing)
<small>_Click to see the full map_</small>

Last year, the week before Christmas, we took a week long roadtrip through the Karoo. Our bigger family car decided to break so we had to take the Spark. This meant getting creative with the packing. At least the Spark has a split back seat, which meant the Boy's chair could still be used while the other half was folded down for more space. Things like the pram making things difficult.

See the above map of all the places we went.

Anyway, on to the trip.

#### The First Day

We drove from home in Ceres and stopped in Matjiesfontein at the Lord Milner. We were trying to make it in time for breakfast at the hotel, but we pulled in just after 10am and they were not keen on letting us in.

So we had breakfast at the coffee house.

![The Spark]({{ "/assets/blog_images/roadtrip_december_2017/matjiesfontein.jpg" | absolute_url }})

Where the birds decided it was also their breakfast time.

Annoyingly when we got back from breakfast the car had a flat tyre which I first had to change. But that didn't take too long and we were soon on our way.

Then we drove the short distance to Laingsburg where we bought some cooldrink. It was impressively hot. Then we turned onto the R323 on our way to Ladismith. 

I turned off onto the dirt road going through the Anysberg reserve which was very pretty.

![The Spark]({{ "/assets/blog_images/roadtrip_december_2017/anysberg.jpg" | absolute_url }})

Some nice rock formations.

![The Spark]({{ "/assets/blog_images/roadtrip_december_2017/anysberg2.jpg" | absolute_url }})

And even a bit of wildlife...

![The Spark]({{ "/assets/blog_images/roadtrip_december_2017/spark.jpg" | absolute_url }})

And one very dirty car!

#### Wolverfontein

Wolverfontein is a wonderful group of cottages located on Plathuis near Ladismith. There are two cottages which are lovely and very quirky.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/wolverfontein5.jpg" | absolute_url }})

The front door. Owls sit on top of the light at night and make a gentle hooting.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/spark2.jpg" | absolute_url }})

Finally the car gets to rest for a bit.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/wolverfontein1.jpg" | absolute_url }})

Looking into the kitchen.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/wolverfontein2.jpg" | absolute_url }})

One of the bathrooms.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/wolverfontein3.jpg" | absolute_url }})

Looking out from the front of the cottage.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/wolverfontein4.jpg" | absolute_url }})

The back of the cottage. Looking up the hill.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/wolverfontein.jpg" | absolute_url }})

One very dirty little boy getting his nappy changed. Travelling with a small child gets a bit interesting sometimes... Though at least the boy enjoys driving and spends most of the time sleeping.

#### Calitzdorp & Meiringspoort

My birthday today :)...

After Wolverfontein we drove through Ladismith, where we stopped and I got a Wimpy coffee.

Then we drove out of Ladismith and onto Calitzdorp for some Port drinking and possibly something to eat.

I didn't take any pictures in Calitzdorp, but we stopped for a tasting at Boplaas where I bought a bottle of Hanneport and a bottle of chocolate Port.

Then we decided to try out De Kraans as well. Both farms came well recommended by the locals. At De Kraans I tasted a whole bunch of Ports and then bought a case. We also had a bit of lunch with a few platters and something cold to drink. We also bought a few jars of preserves and things as gifts.

Then we drove out of Calitzdorp and through Oudtshoorn and De Rust.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/meiringspoort2.jpg" | absolute_url }})

We stopped and had a picnic in Meiringspoort, there are a lot of very nice places to stop along the way.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/meiringspoort.jpg" | absolute_url }})

#### Prince Albert

We ended up in Prince Albert quite late in the afternoon. It was still awfully hot. We stayed at the Huis Adriaan guesthouse. It is a nice and small cottage. The owner is also very friendly.

After unpacking the car we took a bit of a nap, or at least tried to as the Boy was not so keen on resting, having slept almost all the way in the car.

As it was my birthday, the Wife said she was going to spoil me. We were going out for dinner.

We went to the Gallery, which is a very very nice venue. The food is amazing.

Prince Albert has a lot of fun street art, especially near the Gallery.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/pa_1.jpg" | absolute_url }})

A boy trying to get water out of a pipe.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/pa_2.jpg" | absolute_url }})

The cat better watch out...

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/pa_bin.jpg" | absolute_url }})

All of the dirtbins are painted differently. It makes the town look quite nice.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/pa_birthday2.jpg" | absolute_url }})

The Wife had tempura vegetables and seafood as a starter.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/pa_birthday3.jpg" | absolute_url }})

I had tomato and red pepper soup with bread.

Both the starters were very good, I enjoyed the soup but we both agreed if we made it ourselves we would make it a little bit sweeter.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/pa_birthday1.jpg" | absolute_url }})

I went traditional and had a lamb shank which was so so so good.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/pa_birthday4.jpg" | absolute_url }})

The Wife had spinach cannelloni.

Then for pudding.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/pa_birthday5.jpg" | absolute_url }})

Amarula creme Brulee.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/pa_birthday6.jpg" | absolute_url }})

Lemon cake.

We also had the most wonderfully fresh watermelon sorbet.

The boy slept through all of this, which was very nice of him.

#### KFC in Beaufort West

We drove the 40 or so Kms from Prince Albert to Prince Albert Road much quicker than we thought and our booking for the guest house in Merweville was only for 2pm.

The car needed a new spare tyre so we drove through to Beaufort West as it seemed the best place to find a shop. It would also help to kill the time. I had also never driven that part of the N1 and was interested to see what it looked like.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/kfc_beaufort_west.jpg" | absolute_url }})

We even got a few drops of rain, which was very nice. It helped to cool things down for a bit.

#### Merweville

Merweville is a strange little place. We didn't think it was quaint, though it is nice and quiet, but then we also didn't spend long enough or walk around much. The Boy was very niggly so we stayed in our guest house pretty much the whole time.

The second night we were in Merweville, and the boy still wouldn't sleep I decided we should take a drive. We got petrol at Prince Albert Road and then ended up driving all the way back to Prince Albert where we found a very nice restaurant at the sports club, and we bought takeaway pizza.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/merweville.jpg" | absolute_url }})

Looking down into Merweville from the single tarred road into the town.

![Time to rest]({{ "/assets/blog_images/roadtrip_december_2017/merweville_church.jpg" | absolute_url }})

The big church on the main street.

We left Merweville very early in the morning and drove all the way back through Laingsburg and stopped in Matjiesfontein again. This time we were in time for breakfast at the hotel. We had the Rawson Breakfast which is always brilliant.

We really enjoyed our roadtrip holiday and are planning another one at the end of 2018.
