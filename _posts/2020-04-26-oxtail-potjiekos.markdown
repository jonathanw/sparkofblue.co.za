---
layout: post
title:  "Oxtail Potjiekos"
date:   2020-04-26 18:13:00 +0200
categories: blog food cooking oxtail potjiekos
author: jonathan
---
![Perfect Bowl]({{ "/assets/blog_images/oxtail_potjiekos/IMG_6077 4.jpeg" | absolute_url }})

Public holiday during COVID-19 lock down. With nothing else to do I made oxtail potjiekos.

<div class="recipe_block" markdown="1">

**Recipe:**

- 1 kg Oxtail meat
- 60 ml Vegetable oil
- 200 g bacon
- 6 large potatoes
- 2 small butternuts
- 3 small beetroots
- 3 medium onions
- 3 chicken stock cubes
- 3 bay leaves
- 1 teaspoon of crushed garlic
- Half a bottle of wine (white or red is fine)
- 1 litre of boiling water
- 60 ml chutney
- 60 ml tomato sauce
- 10 ml worcestershire sauce
- 3 teaspoons sugar
- Salt & pepper to taste
- Extra water to fill up pot later

**Method**

_I have made this recipe on a fire in a cast iron pot, but it will work just as well on a gas/electric stove indoors with any other sort of pot, just adjust where necessary._

* Start pre-heating your cast iron pot
* Prep the vegetables: peel and dice the potatoes, butternuts, beetroots, and onions
* When the pot is warm add in the vegetable oil
* Dice the bacon and add it to the hot oil. Fry until brown and just turning crispy
* Remove the bacon from the oil and put to one side
* Place the oxtail in the bacon oil and brown on all sides. Then put to one side with the bacon
* Put all of the vegetables into the pot and stir until they are covered in the oil and starting to brown slightly.
* Add the oxtail and bacon back into the pot with the vegetables
* Mix the 3 stock cubes, bay leaves, sugar, worcestershire sauce, tomato sauce, chutney, salt and pepper with the 1 litre of boiling water
* Add the stock mixture to the pot
* Pour in the wine until the meat and vegetables are covered
* Stir round so everything is mixed
* Put the lid on the pot and bring to a boil
* Once boiling, reduce the heat and let the pot simmer gently for 3 - 4 hours. The longer the better
* This is where hopefully you have a few more bottles of wine, and you relax till the pot is done
* Serve on rice or mashed potato, a small side salad will also add some freshness

</div>

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_5995 4.jpeg" | absolute_url }})

Peel, peel, peel...

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_5996 4.jpeg" | absolute_url }})

Wash, wash, wash...

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_5998 4.jpeg" | absolute_url }})

Getting everything nice and warm.

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_6009 4.jpeg" | absolute_url }})

Long way to go still...

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_6011 4.jpeg" | absolute_url }})

Starting to look like something decent.

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_6020 4.jpeg" | absolute_url }})

I smell your cooking, human!

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_6025 4.jpeg" | absolute_url }})

Some more fire, got to keep those coals coming

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_6030 4.jpeg" | absolute_url }})

And more fire.

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_6033 4.jpeg" | absolute_url }})

And even more fire...

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_6043 4.jpeg" | absolute_url }})

Is it done yet?

![Finished Oxtail Potjie]({{ "/assets/blog_images/oxtail_potjiekos/IMG_6064 4.jpeg" | absolute_url }})

Done, done, done...

Eat with rice or mashed potato.