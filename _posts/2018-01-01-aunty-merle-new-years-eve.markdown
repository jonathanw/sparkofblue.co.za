---
layout: post
title:  "Aunty Merle, New Years Eve"
date:   2018-01-01 12:20:00 +0200
categories: blog theatre marc-lottering baxter cape-town
author: jonathan
---
![Us! :)]({{ "/assets/blog_images/aunty_merle_new_years_eve_2017/auntymerle.jpg" | absolute_url }})

The wife and I went to see Marc Lottering's new musical, Aunty Merle, on New Years eve last year. We thoroughly enjoyed the show, it is spellbinding.

We couldn't get hold of the real Aunty Merle for a photo so we had to make do with a cardboard cutout.

![Balloons.]({{ "/assets/blog_images/aunty_merle_new_years_eve_2017/balloons.jpg" | absolute_url }})

Champagne and smooches under the balloons at midnight. 
