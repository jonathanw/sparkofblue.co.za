---
layout: post
title:  "Freedom Day Weekend Roadtrip"
date:   2018-05-01 08:26:00 +0200
categories: blog travel roadtrip drive april holiday leave family
author: jonathan
---
Friday the 27th of April was Freedom Day in South Africa. The farm we 
live on and where the Wife works moved the Workers Day public holiday 
to Monday the 30th of April. So we had a nice and long weekend.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/map.png" | absolute_url }})

The Wife said she didn't want to know where we were going, she wanted 
a surprise. So I spent the last month keeping an eye out for nice 
places to stay. I thought about the places we stayed back in December, 
but that was boring and also they were fully booked already.

Using my favourite app, LekkeSlaap, I searched high and low throughout 
the Western Cape, Northern Cape, and Eastern Cape before finding a 
very nice farm a few kilometers outside of Jourbertina. 

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/stitch2.jpg" | absolute_url }})

The farm is Mount Africa and it is run by a very nice couple. It is 
a working sheep farm. They had just had more than 30 lambs born in 
the week or so before we arrived.

The drive was very long. Google Maps provided two possible routes.
The first one, which was very slightly shorter, went up the N1 to 
Prince Albert Road and then turned down. The other followed Route 
62 mostly going through places like Robertson, Swellendam, 
Mosselbaai, George...

I decided to take the scenic route as we have not seen a lot of the 
places and I like a good drive.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/pizza.jpg" | absolute_url }})

We stopped for lunch in Swellendam where we had the most amazing pizza 
at the Woodpecker Pizzadeli. The Boy got to run around and stretch his 
legs a bit and we had a very nice lunch.

After lunch it was around 2PM and we still had a very long way to go. 
Google maps said another 4 hours.

We drove through Riversdale, Mosselbaai, and George. Then we took the Outeniqua Pass which is very beautiful. Then it was pretty much just straight down the R62 to Joubertina.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/border.jpg" | absolute_url }})

Crossing the border... My first time in the Eastern Cape :).

It was getting dark by the time we got near the farm. The car needed petrol so I drove past to Joubertina as I was not sure exactly where the farm was and did not want to run out. We got petrol at the Total and then also got pizza at the County Apple Takeaway at the Joubs center. The pizza was very good.

Google Maps said there was a way to get to the farm from Jourbertina so we took a dirt road. Turns out Google took us to the neighbouring farm, Grootnek, so we drove around there and then ended up getting to the right place.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/krakeel.jpg" | absolute_url }})

The farm is in Krakeelrivier which is a cute little town. To me it feels very much like a little English town. The houses are very close together and the streets are tiny.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/rain.jpg" | absolute_url }})

On Saturday morning we woke up to a light rain. It rained like that until at least lunch time. It was very nice.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/puddle.jpg" | absolute_url }})

You just can't keep them out of puddles can you?

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/cottage.jpg" | absolute_url }})

Looking over to the cottage from the farm house.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/farmhouse.jpg" | absolute_url }})

Looking back up to the farm house from our cottage.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/tree.jpg" | absolute_url }})

I don't know why but I find trees like this quite beautiful.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/sheep.jpg" | absolute_url }})

Mount Africa is a working farm. They have a lot of sheep. The week before we stayed they had about 36 lambs born. So we got to see some of them being fed which was fun.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/moresheep.jpg" | absolute_url }})

Even more sheep.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/dog.jpg" | absolute_url }})

There are a lot of dogs on the farm but they are very friendly.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/brokencottage.jpg" | absolute_url }})

The owners are working on another broken down building. They are converting it into a third guest cottage. Apparently all the buildings looked like that when they bought the farm a few years ago.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/kitchen.jpg" | absolute_url }})

I forgot to take a picture of the inside before we started unpacking and the boy threw chips everywhere. But the cottage is very nicely furnished and layed out.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/barn.jpg" | absolute_url }})

The barn behind us.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/moretree.jpg" | absolute_url }})

Even more sheep in the distance.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/dark.jpg" | absolute_url }})

The boy didn't want to sleep so we let him run around a bit.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/moon.jpg" | absolute_url }})

The moon rising over the trees. This picture does not do it justice...

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/booze.jpg" | absolute_url }})

Sometimes when you travel with a very small child you just need to sit down and have a couple...

#### Joubertina

On Sunday we took a bit of a drive around. We wanted to see 
if there was a padstal open where we could buy some gifts. 
But it seems nothing is open in this part of the world on a Sunday.

There is not much to Joubertina, you can drive through the town pretty 
quickly. We stopped to get some groceries at the Royal store.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/church.jpg" | absolute_url }})

The church in Jourbertina

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/sonic.jpg" | absolute_url }})

This poor car spends very time being clean...

It was still early so we took a drive down the R62 to see if there was anything interesting.

#### Kareedouw

We turned around in Kareedouw. The boy was sleeping and we were getting tired. Kareedouw looks like a cool town, there seem to be some interesting restuarants that we might want to try sometime.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/kareedouw.jpg" | absolute_url }})

You will have to take their word for it. They were closed...

#### Home again

We got up early and packed up on Monday morning. Checkout time was 10AM. We left around 9. I wanted to get home on the quickest route possible. I was not really feeling like a 6 hour drive home.

We stopped at the Kontrei Kombuis just outside Krakeelrivier to get some jams and preserves for our friends and we also got some bacon and cheese roosterkoeke for breakfast. They were very very good. Made fresh on the fire outside.

![Eastern Cape]({{ "/assets/blog_images/freedom_day_weekend/weskaap.jpg" | absolute_url }})

Google suggested that we drive up to Prince Albert Road and then take the N1 all the way to Touwsrivier. I didn't feel up to the N1. So we drove through De Rust and Oudtshoorn where the stop-go roadworks are still busy. We got stuck there last time in December...

I filled the car with petrol in Calitzdorp and then we continued on to Ladismith. I should have taken the Seweweekspoort turn off before Zoar, but I thought there was another tarred road from Ladismith to Laingsburg. But I was wrong and we ended up taking the same dirt road through Anysberg as we did in December.

In Laingsburg we got lunch at the Wimpy at the Engen garage. Then we went to visit the Wife's parents grave and left some flowers. Then we took the N1 to Ceres, turning off onto the R46 at Touwsrivier. The drive was mostly quiet, which was nice. There was not a lot of traffic. I assume everyone only started thinking about going home on Monday evening or Tuesday.

Next year we are thinking about doing a boat trip from Durban, so we will definitely be staying at Mount Africa again on our way up. It really was a wonderful getaway.