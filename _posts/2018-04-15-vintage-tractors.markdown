---
layout: post
title:  "Vintage Tractor Drive (2017)"
date:   2018-04-15 22:19:00 +0200
categories: blog farm vintage tractors
author: jonathan
---
![Me!]({{ "/assets/blog_images/tractor_day/22793932_125009024851687_6905462432987086848_n.jpg" | absolute_url }})

These are some photos I found from the farm's vintage tractor drive last year in October. I took a load more with my film camera but have still not had the film developed, I need to do that soon. Where I do I will post more photos.

The wife works very hard every year around October to organise the Vintage tractor drive for the Kaleo Market day.

![No Photos Allowed]({{ "/assets/blog_images/tractor_day/22794125_124443508246718_6066501465759809536_n.jpg" | absolute_url }})

How can you not take a photo of this shirt?

![Tractors]({{ "/assets/blog_images/tractor_day/22793844_985539768252118_7020199679686082560_n.jpg" | absolute_url }})

Parked all in a row.

![Tractors]({{ "/assets/blog_images/tractor_day/22802631_1775131846112760_3087636973888733184_n.jpg" | absolute_url }})

Nice old John Deere.

![Me!]({{ "/assets/blog_images/tractor_day/22801963_1853430444987427_8621642196652654592_n.jpg" | absolute_url }})

Look at me.

![Me!]({{ "/assets/blog_images/tractor_day/22793932_125009024851687_6905462432987086848_n.jpg" | absolute_url }})

Vintage tractors spend a lot of time being parked :).

![Me!]({{ "/assets/blog_images/tractor_day/22793969_296751750831502_9036656631537991680_n.jpg" | absolute_url }})

This very pretty International was very carefully restored a couple of years ago.

![Me!]({{ "/assets/blog_images/tractor_day/22860947_287061095114371_4238255663060353024_n.jpg" | absolute_url }})




