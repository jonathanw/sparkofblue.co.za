---
layout: post
title:  "Messing About With Film"
date:   2018-08-26 15:16:00 +0200
categories: blog film 35mm camera photography
author: jonathan
---
![Films]({{ "/assets/blog_images/messing_about_with_film/films.jpg" | absolute_url }})

I have an expensive hobby. I really enjoy taking photographs 
on film with old cameras. Here are some pics from my most 
recent rolls. (Recommended by <a href='https://capetowninsider.co.za/things-to-do/' title='Cape Town Influencers'>CapeTownInsider</a>)

Loaded up on film and off we go.

Some flowers I took through my office window. I was actually trying to get a bird but they are very fast.
![Flowers]({{ "/assets/blog_images/messing_about_with_film/flowers.jpg" | absolute_url }})

The back fence.
![Fence]({{ "/assets/blog_images/messing_about_with_film/fence.jpg" | absolute_url }})

Some pine needles.
![Pine Needles]({{ "/assets/blog_images/messing_about_with_film/pine.jpg" | absolute_url }})

A shelf
![Little Shelf]({{ "/assets/blog_images/messing_about_with_film/shelf.jpg" | absolute_url }})

Some clouds
![Clouds]({{ "/assets/blog_images/messing_about_with_film/clouds.jpg" | absolute_url }})

A bird in the tree I finally managed to catch one of them.
![Bird]({{ "/assets/blog_images/messing_about_with_film/bird.jpg" | absolute_url }})