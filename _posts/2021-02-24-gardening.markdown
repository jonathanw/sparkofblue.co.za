---
layout: post
title:  "Gardening"
date:   2021-02-24 10:58:00 +0200
categories: blog garden jalapeno plant gardening chillies peppers hot
author: jonathan
---
![Jalapeno Flowering]({{ "/assets/blog_images/gardening/jalapenos.jpeg" | absolute_url }})

My Jalapenos have started making flower buds. Hopefully in a few months there will be some nice hot peppers.