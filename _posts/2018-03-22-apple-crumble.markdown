---
layout: post
title:  "Apple Crumble"
date:   2018-03-22 11:16:00 +0200
categories: blog recipe apples farm crumble yum
author: jonathan
---
When you live on a fruit farm you end up having a 
huge amount of fruit lying around the house. All of 
this fruit tends to go off really quickly. Especially 
with the warm weather we've been having.

For a couple of reasons we had 3 boxes of fruit that has 
been sitting at the front door for the last two weeks. 
My long suffering wife has been asking me for ages now to 
look in the boxes and deal with the rot.

Last night at around 10PM I decided it was time to deal 
with the fruit. I could save almost all of the apples in 
the one box, there was one box i was too scared to open 
and the last box contained yellow cling peaches which I 
could also mostly save.

There were also a few nectarines but they are getting 
really soft now. I will do something with them next.

I have been wanting to make some sort of apple pie for 
while now and last night seemed the right time. Otherwise 
the remaining apples will just also go off.

A crumble seemed like the simplest. I peeled a pot full of 
apples, cut them into quarters, and took out the cores. 

I washed the bits, then put them in a pot with sugar, 
cinnamon, a largish slash of brandy, a tiny bit of ginger 
and some raisins.

I let the apples boil down until they were soft, then I 
made the crumble. The crumble is really simple. You 
really don't need a recipe for this, just do what you 
feel is right.

* 700 grams, flour
* 200 grams, butter or margarine
* 250 grams, unrefined brown sugar

Cut the butter into small cubes and then put the 
flour, sugar, and butter into a mixing bowl. If the butter 
is cold you will just need to work a little bit harder.

You can mix the mixture with your hands or in a mixer 
depending on your mood. When the mixture is all combined 
and looks a little bit like breadcrumbs you are done. Put 
the soft apples into a baking disk - include some of the 
apple liquid, it should just cover the bottom of the dish, but 
if you want a wetter crumble at the end add in more liquid.

Put the crumble on top of the apples evenly and then throw the 
whole lot into the oven. 180 C for about 45 minutes should do 
it. Depending on the size of your crumble.
