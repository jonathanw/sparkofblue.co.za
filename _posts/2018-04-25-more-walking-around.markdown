---
layout: post
title:  "More Walking Around"
date:   2018-04-25 10:17:00 +0200
categories: blog farm walking outdoors
author: jonathan
---
The farm put in a new laminated wood floor in the house recently. 
While the builders were in the house it made working very 
hard. So I took the boy for a walk in the afternoon.

![Byebye]({{ "/assets/blog_images/more_walking/christopher1.jpg" | absolute_url }})

And he's off...

![Still going]({{ "/assets/blog_images/more_walking/christopher2.jpg" | absolute_url }})

Still going strong.

![Tree]({{ "/assets/blog_images/more_walking/road.jpg" | absolute_url }})

More of the road going past the house.

![Tree]({{ "/assets/blog_images/more_walking/tree.jpg" | absolute_url }})

And some trees :)

![Trees]({{ "/assets/blog_images/more_walking/trees.jpg" | absolute_url }})

Some more trees.

![Bog]({{ "/assets/blog_images/more_walking/bog.jpg" | absolute_url }})

Don't go in there...