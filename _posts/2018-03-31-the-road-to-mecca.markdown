---
layout: post
title:  "The Road to Mecca"
date:   2018-03-31 23:42:00 +0200
categories: blog cape-town fugard theatre
author: jonathan
---
![The Road to Mecca]({{ "/assets/blog_images/29402282_1896249673718697_1171066822309445632_n.jpg" | absolute_url }})

There are no words... The Road to Mecca is a brilliant show. I would highly recommend going to see it at the Fugard Theatre in Cape Town.

